USE SuperheroesDB;

CREATE TABLE SuperheroPowerLink (
SuperheroLinkID int CONSTRAINT FK_Superhero_Superpower FOREIGN KEY REFERENCES Superhero(HeroID) not null,
PowerLinkID int CONSTRAINT FK_Superpower_Superhero FOREIGN KEY REFERENCES Superpower(PowerID) not null
)
GO

ALTER TABLE SuperheroPowerLink
ADD CONSTRAINT PK_SuperheroPower PRIMARY KEY (SuperheroLinkID, PowerLinkID)
GO

