USE SuperheroesDB;

CREATE TABLE Superhero (
HeroID int PRIMARY KEY IDENTITY(1,1) not null,
Name varchar(50) not null,
Alias varchar(50) not null,
Origin varchar(50) not null
);

CREATE TABLE Assistant (
AssistantID int PRIMARY KEY IDENTITY(1,1) not null,
Name varchar(50) not null
);

CREATE TABLE Superpower (
PowerID int PRIMARY KEY IDENTITY(1,1) not null,
Name varchar(50) not null,
Description varchar(50) not null
);
