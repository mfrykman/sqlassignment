USE SuperheroesDB;

INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Pimfex', 'Wizboy', 'Third Moon of Saturn');
INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Li Qung', 'Blood Samurai', 'Quinong Fey');
INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Jaques', 'Baguette-Man', 'Paris, France');