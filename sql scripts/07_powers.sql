USE SuperheroesDB;

INSERT INTO Superpower (Name, Description)
VALUES ('Fireball', 'Shoots ball of fire');
INSERT INTO Superpower (Name, Description)
VALUES ('Invisibility', 'Makes hero invisible');
INSERT INTO Superpower (Name, Description)
VALUES ('Flying', 'The hero can fly');
INSERT INTO Superpower (Name, Description)
VALUES ('Frostbolt', 'Shoots bolt of frost');

INSERT INTO SuperheroPowerLink (SuperheroLinkID, PowerLinkID)
VALUES (1,1);
INSERT INTO SuperheroPowerLink (SuperheroLinkID, PowerLinkID)
VALUES (1,2);
INSERT INTO SuperheroPowerLink (SuperheroLinkID, PowerLinkID)
VALUES (2,1);
INSERT INTO SuperheroPowerLink (SuperheroLinkID, PowerLinkID)
VALUES (3,4);

