﻿using SQLClient_Assignment5.Models;
using SQLClient_Assignment5.Repositories;
using System;
using Microsoft.Data.SqlClient;
using System.Data;

namespace SQLClient_Assignment5
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();

            // PrintAllCustomers(repository);

            // PrintCustomerById(repository);

            // InsertCustomer(repository);

            DeleteCustomer(repository);
        }


        //Print all customers
        static void PrintAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }

        //Print customer by id
        static void PrintCustomerById(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomer(25));
        }

        //Insert new customer
        static void InsertCustomer(ICustomerRepository repository)
        {
            Customer customer = new Customer()
            {
                FirstName = "John",
                LastName = "Johnson",
                Country = "Denmark",
                PostalCode = "2300",
                Phone = "60158093",
                Email = "john.johnson@yahoo.com"
            };
            if (repository.AddNewCustomer(customer))
            {
                Console.WriteLine("It works!");
            }
            else
            {
                Console.WriteLine("Not working");
            }
        }

        static void UpdateCustomer(ICustomerRepository repository)
        {
         
        }

        static void DeleteCustomer(ICustomerRepository repository)
        {
            repository.DeleteCustomerById(59);
        }

        
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        
        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} - {customer.FirstName} {customer.LastName} - {customer.Country} - {customer.PostalCode} - {customer.Phone} - {customer.Email} ---");
        }
    }

}