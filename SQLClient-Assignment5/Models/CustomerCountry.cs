﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLClient_Assignment5.Models
{
    public class CustomerCountry
    {
        public int CountryCount { get; set; }
        public string? Country { get; set; }
    }
}